﻿#include <iostream>


class Vector
{
public:
	Vector() : x(5), y(2), z(6)
	{}
	Vector(double _x,double _y,double _z) : x(_x),y(_y),z(_z)
	{}
	void Show()
	{
		std::cout << x << ' ' << y << ' ' << z;
	}
	void Show_Z()
	{
		std::cout << z;
	}
	void Show_Long()
	{
		int Long = sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
		std::cout << Long;
	}
private:
		double x;
		double y;
		double z;
};

int main()
{
	Vector w;
	w.Show_Long();
}